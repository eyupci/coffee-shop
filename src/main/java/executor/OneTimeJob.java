package executor;

import java.util.concurrent.TimeUnit;

/**
 * Job to be executed just once.
 */
public abstract class OneTimeJob extends Job {

    /**
     * Construct setting the start delay in the given time unit.
     *
     * @param timeSet start delay
     * @param timeUnit time unit
     */
    public OneTimeJob(final int timeSet, final TimeUnit timeUnit) {
        super(timeSet, 0, timeUnit);
    }

    /**
     * Construct setting the start delay in seconds.
     *
     * @param timeSet start delay in seconds
     */
    public OneTimeJob(final int timeSet) {
        super(timeSet, 0, TimeUnit.SECONDS);
    }

    /**
     * Construct starting the Job immediately.
     */
    public OneTimeJob() {
        super(0, 0, TimeUnit.SECONDS);
    }
}