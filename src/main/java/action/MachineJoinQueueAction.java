package action;

import executor.OneTimeJob;
import model.Programmer;
import type.ProgrammerEventType;

/**
 * Programmer joins the machine queue.
 */
public class MachineJoinQueueAction extends ProgrammerAction{

    public MachineJoinQueueAction(Programmer programmer){
        super(programmer, ProgrammerEventType.MACHINE_JOIN_QUEUE);
    }

    @Override
    public void process() {
        programmer.getCoffeeShop().getCoffeeMachine().addQueue(programmer);
        printInfo();
    }
}
