package model;

import action.CassierAction;
import executor.ExecutorManager;
import type.PaymentType;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Holds cassier abstraction to gather payments
 */
public final class Cassier {

    /**
     * Holds maximum number of payments at the same time.
     */
    private int maxNumberofPayments;

    /**
     * Holds payment queue.
     */
    private final LinkedList<Programmer> paymentQueue = new LinkedList<>();

    /**
     * Holds payment lock to prevent race condition.
     */
    private final Object paymentLock = new Object();

    /**
     * Holds current payment count.
     */
    private final AtomicInteger currentPaymentCount = new AtomicInteger();

    /**
     * Holds payment types.
     */
    private static final List<PaymentType> paymentTypes = Arrays.asList(PaymentType.values());

    /**
     * Periodic cassier action to get the payments
     */
    private  CassierAction cassierAction;

    public Cassier(int maxNumberOfPayments) {
        this.cassierAction = new CassierAction(this);
        this.maxNumberofPayments = maxNumberOfPayments;
        ExecutorManager.getInstance().addTask(cassierAction);
    }

    /**
     * Adds programmer to payment queue.
     * @param programmer
     */
    public void addQueue(Programmer programmer) {
        synchronized (paymentLock) {
            this.paymentQueue.add(programmer);
        }
    }

    public void processPayment() {
        synchronized (paymentLock) {
            if (currentPaymentCount.get() < maxNumberofPayments && paymentQueue.size() > 0) {
                Programmer programmer =  paymentQueue.removeFirst();

                if (programmer != null) {
                    currentPaymentCount.incrementAndGet();

                    //randomly select payment.
                    programmer.setSelectedPaymentType(paymentTypes.get(new Random().nextInt(paymentTypes.size())));

                    //go for next action.
                    programmer.nextAction();
                }
            }
        }
    }

    public void pay(Programmer programmer) {
        currentPaymentCount.decrementAndGet();
    }
}
