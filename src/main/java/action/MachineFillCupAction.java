package action;

import model.Programmer;
import type.ProgrammerEventType;

/**
 * Action to fill the cup.
 */
public class MachineFillCupAction extends ProgrammerAction{

    public MachineFillCupAction(Programmer programmer) {
        super(programmer, ProgrammerEventType.MACHINE_WAIT_TILL_FILL,
                programmer.getSelectedCoffeeType().getElapsedTime());
    }
}
