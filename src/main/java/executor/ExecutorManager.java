package executor;

import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Holds general thread pool executor.
 */
public final class ExecutorManager {

    /**
     * Singleton instance.
     */
    private static final ExecutorManager INSTANCE = new ExecutorManager();

    /**
     * Pool size.
     */
    private static final int POOL_SIZE = 16;

    /**
     * Pool executor.
     */
    private ScheduledThreadPoolExecutor executor;

    private ExecutorManager() {
        executor = new ScheduledThreadPoolExecutor(POOL_SIZE);
    }

    public static ExecutorManager getInstance() {
        return INSTANCE;
    }

    public  void addTask(final Job task) {
        ScheduledFuture job;

        // Schedule it at fix rate.
        if (task.getTimeInterval() > 0) {
            job = this.executor.scheduleAtFixedRate(
                    task,
                    task.getTimeSet(),
                    task.getTimeInterval(),
                    task.getTimeUnit()
            );
        } else {
            job = this.executor.schedule(task, task.getTimeSet(), task.getTimeUnit());
        }
        task.setJob(job);
    }

    /**
     * Removes a task from the pool.
     *
     * @param task task to be removed
     */
    public void removeTask(final Job task) {
        executor.remove(task);
    }

    /**
     * Shutdown the thread pool.
     */
    public void shutdown() {
        executor.shutdown();
    }


}
