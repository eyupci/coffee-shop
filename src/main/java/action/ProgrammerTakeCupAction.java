package action;

import model.Programmer;
import stats.StatsManager;
import type.ProgrammerEventType;

/**
 * Final action of the programmer
 */
public class ProgrammerTakeCupAction extends ProgrammerAction{

    public ProgrammerTakeCupAction(Programmer programmer) {
        super(programmer, ProgrammerEventType.MACHINE_LEAVE);
    }

    @Override
    public void process() {
        programmer.getCoffeeShop().getCoffeeMachine().giveCoffee(programmer);
        super.process();
        StatsManager.getInstance().onProgrammerEnd(programmer.getName());
    }
}
