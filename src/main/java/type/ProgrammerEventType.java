package type;

import action.*;
import model.Programmer;

/**
 * Holds coffee machine event types.
 */
public enum ProgrammerEventType {

    JOIN_COFFEE_QUEUE("joined to coffee queue", 0f) {
        @Override
        public ProgrammerAction getAction(Programmer programmer) {
            return new JoinCoffeeQueueAction(programmer);
        }
    },
    ASK_COFFEE_INFO("asked coffee types", 0.5f) {
        @Override
        public ProgrammerAction getAction(Programmer programmer) {
            return new AskCoffeeAction(programmer);
        }
    },
    PAYMENT_QUEUE("joined to payment queue",0){
        @Override
        public ProgrammerAction getAction(Programmer programmer) {
            return new PaymentQueueAction(programmer);
        }
    },
    PAY("payment done"){
        @Override
        public ProgrammerAction getAction(Programmer programmer) {
            return new PayAction(programmer);
        }
    },
    MACHINE_JOIN_QUEUE("joined machine queue", 0) {
        @Override
        public ProgrammerAction getAction(Programmer programmer) {
            return new MachineJoinQueueAction(programmer);
        }
    },
    MACHINE_FIND_CUP("found cup",0.25f),
    MACHINE_PUT("put it under the outlet",0.25f),
    MACHINE_PICK_TYPE("pick type of coffee",0.25f),
    MACHINE_WAIT_TILL_FILL("wait for the filling the cup") {
        @Override
        public ProgrammerAction getAction(Programmer programmer) {
            return new MachineFillCupAction(programmer);
        }
    },
    MACHINE_LEAVE("take cup and leave", 0.25f) {
        @Override
        public ProgrammerAction getAction(Programmer programmer) {
            return new ProgrammerTakeCupAction(programmer);
        }
    };

    ProgrammerEventType(String eventName, float timeElapsed) {
        this.eventName = eventName;
        this.timeElapsed = timeElapsed;
    }

    ProgrammerEventType(String eventName) {
        this.eventName = eventName;
    }

    /**
     * Holds the event name.
     */
    private String eventName;


    /**
     * Holds time elapsed time.
     */
    private float timeElapsed;

    public float getTimeElapsed() {
        return timeElapsed;
    }

    public String getEventName() {
        return eventName;
    }

    /**
     * Returns action class to be executed.
     * @param programmer
     * @return
     */
    public ProgrammerAction getAction(Programmer programmer) {
        return new ProgrammerAction(programmer, this);
    }
}
