package action;

import executor.PeriodicJob;
import model.CoffeeMachine;

import java.util.concurrent.TimeUnit;

/**
 * Simulates the coffee machine to give coffees.
 */
public class CoffeeMachineAction extends PeriodicJob{

    /**
     * Holds the coffee machine
     */
    private CoffeeMachine coffeeMachine;

    private static final int INTERNAL = 100; // in miliseconds

    public CoffeeMachineAction(CoffeeMachine coffeeMachine) {
        super(0, INTERNAL, TimeUnit.MILLISECONDS);
        this.coffeeMachine = coffeeMachine;
    }

    @Override
    public void process() {
        this.coffeeMachine.processRequest();
    }

}
