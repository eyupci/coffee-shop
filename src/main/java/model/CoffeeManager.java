package model;

import action.CoffeeManagerAction;
import executor.ExecutorManager;
import type.CoffeeType;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Holds the coffee manager to give coffee types.
 */
public class CoffeeManager {

    /**
     * Static coffee types to return.
     */
    private static List<CoffeeType> coffeeTypes;

    /**
     * Static block to set coffee types.
     */
    static {
        coffeeTypes = new ArrayList<>(EnumSet.allOf(CoffeeType.class));
    }

    /**
     * Maximum concurrent programmers.
     */
    private int maxNumberOfProgrammes;

    /**
     * Holds concurrent programmers.
     */
    private AtomicInteger ongoingRequest = new AtomicInteger();

    /**
     * Lock to synchronize.
     */
    private final Object pickCoffeeLock = new Object();

    /**
     * Programmers queue.
     */
    private LinkedList<Programmer> programmerPickCoffeeQueue = new LinkedList<>();

    /**
     * Action to check queue in periodic time.
     */
    private CoffeeManagerAction coffeeManagerAction;


    public CoffeeManager(int maxNumberOfProgrammers) {
        coffeeManagerAction = new CoffeeManagerAction(this);
        ExecutorManager.getInstance().addTask(coffeeManagerAction);
        this.maxNumberOfProgrammes = maxNumberOfProgrammers;
    }

    public void addQueue(Programmer programmer) {
        synchronized (pickCoffeeLock) {
            this.programmerPickCoffeeQueue.add(programmer);
        }
    }

    public void processCoffeeRequest() {
        synchronized (pickCoffeeLock) {
           if (ongoingRequest.get() < maxNumberOfProgrammes && programmerPickCoffeeQueue.size() > 0) {
                Programmer programmer = this.programmerPickCoffeeQueue.removeFirst();
                if (programmer != null) {
                    ongoingRequest.incrementAndGet();
                    programmer.nextAction();
                }
           }
        }
    }

    public List<CoffeeType> askCoffeeTypes(Programmer programmer) {
        synchronized (pickCoffeeLock) {
            ongoingRequest.decrementAndGet();
        }
        return coffeeTypes;
    }
}
