package action;

import event.EventHelper;
import executor.OneTimeJob;
import model.Programmer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import type.ProgrammerEventType;

import java.util.concurrent.TimeUnit;

/**
 * Defines customer actions
 */
public class ProgrammerAction extends OneTimeJob {

    /**
     * Holds the event type.
     */
    protected ProgrammerEventType eventType;

    /**
     * Holds the programmer.
     */
    protected Programmer programmer;

    /**
     * Used for logging.
     */
    private static final Logger LOGGER = LogManager.getLogger(ProgrammerAction.class);

    public ProgrammerAction(Programmer programmer, ProgrammerEventType eventType, float time) {
        super(EventHelper.getInstance().timePass(time), TimeUnit.MILLISECONDS);
        this.programmer = programmer;
        this.eventType = eventType;
    }

    public ProgrammerAction(Programmer programmer, ProgrammerEventType eventType) {
        super(EventHelper.getInstance().timePass(eventType.getTimeElapsed()), TimeUnit.MILLISECONDS);
        this.eventType = eventType;
        this.programmer = programmer;
    }

    protected void printInfo() {
        LOGGER.debug(programmer.getName() + " " + eventType.getEventName() + " has done.");
    }

    @Override
    public void process() {
        printInfo();
        programmer.nextAction();
    }
}
