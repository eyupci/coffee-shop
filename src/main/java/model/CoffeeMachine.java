package model;

import action.CoffeeMachineAction;
import executor.ExecutorManager;
import java.util.LinkedList;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Holds abstraction of coffee machine.
 */
public class CoffeeMachine {

    /**
     * Maximum number of concurrent programmers.
     */
    private int maxNumberOfProgrammers;

    /**
     * Programmers queue.
     */
    private LinkedList<Programmer> queue = new LinkedList<>();

    /**
     * Lock object to syncronize queue.
     */
    private final Object machineLock = new Object();

    /**
     * Holds current programmers on the machine.
     */
    private final AtomicInteger currentProgrammerCount = new AtomicInteger();


    /**
     * Periodic coffee machine action.
     */
    private CoffeeMachineAction coffeeMachineAction;

    public CoffeeMachine(int maxNumberOfProgrammers) {
        this.maxNumberOfProgrammers = maxNumberOfProgrammers;
        this.coffeeMachineAction = new CoffeeMachineAction(this);
        ExecutorManager.getInstance().addTask(coffeeMachineAction);
    }

    /**
     * Adds programmer to payment queue.
     *
     * @param programmer
     */
    public void addQueue(Programmer programmer) {
        synchronized (machineLock) {
            this.queue.add(programmer);
        }
    }

    /**
     * Process coffee machine request.
     */
    public void processRequest() {
        synchronized (machineLock) {
            if (currentProgrammerCount.get() < maxNumberOfProgrammers && queue.size() > 0) {
                Programmer programmer = queue.remove();
                if (programmer != null) {
                    currentProgrammerCount.incrementAndGet();

                    //go for next action.
                    programmer.nextAction();
                }
            }
        }
    }

    /**
     * Give coffee.
     * @param programmer
     */
    public void giveCoffee(Programmer programmer) {
        currentProgrammerCount.decrementAndGet();
    }
}
