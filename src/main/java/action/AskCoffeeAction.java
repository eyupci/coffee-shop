package action;

import model.Programmer;
import stats.StatsManager;
import type.CoffeeType;
import type.ProgrammerEventType;

import java.util.List;
import java.util.Random;

/**
 * Performs ask coffee action.
 */
public class AskCoffeeAction extends ProgrammerAction {

    public AskCoffeeAction(Programmer programmer) {
        super(programmer, ProgrammerEventType.ASK_COFFEE_INFO);
    }

    @Override
    public void process() {
        List<CoffeeType> coffeeTypeList = programmer.getCoffeeShop().getCoffeeManager()
                                    .askCoffeeTypes(programmer);
        CoffeeType selectedCoffeeType = coffeeTypeList.get(new Random().nextInt(coffeeTypeList.size()));
        programmer.setSelectedCoffeeType(selectedCoffeeType);

        super.process();

        StatsManager.getInstance().onUserSelectCoffee(programmer.getName(), selectedCoffeeType);

    }
}
