package model;

import config.Configuration;
import executor.ExecutorManager;
import stats.StatsManager;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Holds coffee shop abstraction.
 */
public class CoffeeShop {

    /**
     * Holds coffee shop configuration.
     */
    private Configuration configuration;

    /**
     * Holds coffee manager.
     */
    private CoffeeManager coffeeManager;

    /**
     * Holds cassier to get payment.
     */
    private Cassier cassier;

    /**
     * Holds coffee machine.
     */
    private CoffeeMachine coffeeMachine;

    /**
     * Tracks the programmers to finish their coffee taking.
     */
    private AtomicInteger programmerDoneCount = new AtomicInteger(0);

    public CoffeeShop(Configuration configuration) {
        this.configuration = configuration;
    }

    public void start() {

        //initialize coffee manager, cassier and machine
        this.coffeeManager = new CoffeeManager(configuration.getMaxNumberOfPickCoffee());
        this.cassier = new Cassier(configuration.getMaxNumberOfPayment());
        this.coffeeMachine = new CoffeeMachine(configuration.getMaxNumberOfGetCoffee());

        //run the programmers to get their coffee.

        System.out.println("Coffee Shop is started...");

        for (int i=0; i<configuration.getNumberOfProgrammers(); i++) {
            Programmer programmer = new Programmer(this, i);
            programmer.init();
            ExecutorManager.getInstance().addTask(programmer);
        }

        //wait them to take their coffees.
        while (!checkEnd()) {
            // keep continue
        }

        //close shop.
        closeShop();
    }

    /**
     * Returns coffee manager (responsible from giving the coffee types to programmer)
     * @return
     */
    public CoffeeManager getCoffeeManager() {
        return coffeeManager;
    }

    /**
     * Returns cassier (responsible from payment)
     * @return
     */
    public Cassier getCassier() {
        return cassier;
    }

    /**
     * Returns coffee machine (responsible from giving coffee)
     * @return
     */
    public CoffeeMachine getCoffeeMachine() {
        return coffeeMachine;
    }

    /**
     * Each programmer will call the function to indicate job is done.
     */
    public void setDone() {
        programmerDoneCount.incrementAndGet();
    }

    /**
     * Check the program whether finished or not.
     * @return
     */
    private boolean checkEnd() {
        if (programmerDoneCount.get() == configuration.getNumberOfProgrammers()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Close the shop. Display statistics.
     */
    private void closeShop() {
        StatsManager.getInstance().displayStats(configuration.getNumberOfProgrammers());
        ExecutorManager.getInstance().shutdown();
    }
}
