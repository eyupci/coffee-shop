import config.Configuration;
import model.CoffeeShop;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;

/**
 * Main starter class.
 */
public class Main {

    static {
        System.setProperty("log4j.configurationFile", System.getProperty("user.dir")
                + File.separator + "log4j.xml");
    }

    /**
     * Used for logging.
     */
    private static final Logger LOGGER = LogManager.getLogger(Main.class);

    /**
     * Holds the default concurrent pick coffee programmers.
     */
    private static final int DEFAULT_CONCURRENT_PICK_COFFEE = 10;

    /**
     * Holds the default concurrent payment.
     */
    private static final int DEFAULT_CONCURRENT_PAYMENT = 5;

    /**
     * Holds the default concurrent get coffee from the machine.
     */
    private static final int DEFAULT_CONCURRENT_GET_COFFEE = 2;

    /**
     * Holds the default concurrent users.
     */
    private static final int DEFAULT_USERS = 100;

    /**
     * Application starter.
     * @param args command line params
     */
    public static void main(String[] args) {

        int programmerCount = DEFAULT_USERS;
        if (args.length > 0) {
            programmerCount = Integer.parseInt(args[0]);
        }

        LOGGER.debug("Coffee shop is opened with " + programmerCount + " users");

        Configuration configuration = new Configuration(
                DEFAULT_CONCURRENT_PICK_COFFEE, DEFAULT_CONCURRENT_PAYMENT,
                DEFAULT_CONCURRENT_GET_COFFEE, programmerCount);

        CoffeeShop coffeeShop = new CoffeeShop(configuration);
        coffeeShop.start();
    }
}
