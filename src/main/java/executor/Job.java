package executor;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * Base Job object.
 */
public abstract class Job implements Runnable {

    /**
     * Used for logging.
     */
    private static final Logger LOGGER = LogManager.getLogger(Job.class);

    /**
     * Holds the job to be executed.
     */
    private ScheduledFuture job;

    /**
     * Holds the delay time for which the job was set.
     */
    private int timeSet;

    /**
     * Holds the time unit in which the delay is being measured.
     */
    private TimeUnit timeUnit;

    /**
     * Holds the interval at which the job should be executed.
     */
    private int timeInterval;

    /**
     * Default construct setting time, interval and time unit.
     *
     * @param timeSet time delay of job
     * @param timeInterval interval for execution
     * @param timeUnit time unit to measure the delay
     */
    public Job(final int timeSet, final int timeInterval, final TimeUnit timeUnit) {
        this.timeSet = timeSet;
        this.timeInterval = timeInterval;
        this.timeUnit = timeUnit;
    }

    /**
     * Returns the time interval for the job.
     *
     * @return time interval
     */
    public final int getTimeInterval() {
        return timeInterval;
    }

    /**
     * Returns the start delay for the job.
     *
     * @return start delay
     */
    public final int getTimeSet() {
        return timeSet;
    }

    /**
     * Returns the time unit in which the start delay is being measured.
     *
     * @return time unit for delay
     */
    public final TimeUnit getTimeUnit() {
        return timeUnit;
    }

    /**
     * Sets the job to be executed.
     *
     * @param future job to be executed
     */
    public final void setJob(final ScheduledFuture<?> future) {
        this.job = future;
    }

    /**
     * Runs the assigned job.
     */
    public final void run() {
        try {
            this.process();
        } catch (Exception ex) {
            LOGGER.error("error while executing job", ex);
            ex.printStackTrace();
        }
    }

    /**
     * Cancels the current job.
     *
     * todo: check for clean interruption
     */
    public final void cancel() {
        job.cancel(false);
        ExecutorManager.getInstance().removeTask(this);
    }

    /**
     * Checks the current job is cancelled.
     * @return boolean
     */
    public final boolean isCancelled() {
        return job.isCancelled();
    }

    /**
     * Processes the data for the assigned job.
     */
    public abstract void process();
}