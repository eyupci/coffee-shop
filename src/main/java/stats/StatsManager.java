package stats;

import type.CoffeeType;
import type.PaymentType;

import java.util.EnumMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Holds stats of the programmers
 */
public final class StatsManager {

    private static final StatsManager instance = new StatsManager();

    private ConcurrentHashMap<String, Stats> programmerStats = new ConcurrentHashMap<>();

    /**
     * Avoid initialization.
     */
    private StatsManager(){

    }

    public static StatsManager getInstance() {
        return instance;
    }

    public void onProgrammerStart(String name) {
        Stats userStats = new Stats(name);
        userStats.setStartTime(System.currentTimeMillis());
        programmerStats.put(name, userStats);
    }

    public void onUserSelectCoffee(String name, CoffeeType coffeeType) {
        programmerStats.get(name).setCoffeeType(coffeeType);
    }

    public void onUserPay(String name, PaymentType paymentType) {
        programmerStats.get(name).setPaymentType(paymentType);
    }

    public void onProgrammerEnd(String name) {
        programmerStats.get(name).setEndTime(System.currentTimeMillis());
    }

    public void displayStats(int numberOfProgrammers) {
        System.out.println("Number of programmers:" + numberOfProgrammers);

        EnumMap<PaymentType, AtomicInteger> perSoldByPaymentType = new EnumMap<>(PaymentType.class);
        for (PaymentType paymentType : PaymentType.values()) {
            perSoldByPaymentType.put(paymentType, new AtomicInteger());
        }

        EnumMap<CoffeeType, AtomicInteger> perDispensedCoffeeType = new EnumMap<>(CoffeeType.class);
        for (CoffeeType coffeeType : CoffeeType.values()) {
            perDispensedCoffeeType.put(coffeeType, new AtomicInteger());
        }

        long totalAmountOfTime = 0;
        long minAmountOfTime = Long.MAX_VALUE;
        String minAmountOfTimeUser = null;
        long maxAmountOfTime = 0;
        String maxAmountOfTimeUser = null;
        for (Stats stat : programmerStats.values()) {
            perSoldByPaymentType.get(stat.getPaymentType()).incrementAndGet();
            perDispensedCoffeeType.get(stat.getCoffeeType()).incrementAndGet();
            totalAmountOfTime += stat.getAmountOfTime();

            if (maxAmountOfTime < stat.getAmountOfTime()) {
                maxAmountOfTime = stat.getAmountOfTime();
                maxAmountOfTimeUser = stat.getName();
            }

            if (minAmountOfTime > stat.getAmountOfTime()) {
                minAmountOfTime = stat.getAmountOfTime();
                minAmountOfTimeUser = stat.getName();
            }
        }

        System.out.println("Number of coffee sold per payment method: ");
        for (PaymentType paymentType : PaymentType.values()) {
            System.out.println(paymentType.getName() + ": " + perSoldByPaymentType.get(paymentType));
        }

        System.out.println("Number of coffee sold per coffee type: ");
        for (CoffeeType coffeeType : CoffeeType.values()) {
            System.out.println(coffeeType.getName() + ": " + perDispensedCoffeeType.get(coffeeType));
        }

        float average = (float) totalAmountOfTime / numberOfProgrammers;
        System.out.println("Average waiting time: " + average + " ms.");

        System.out.println("Min amount of time: " + minAmountOfTime + " ms. (" + minAmountOfTimeUser + ")");

        System.out.println("Max amount of time: " + maxAmountOfTime + " ms. (" + maxAmountOfTimeUser + ")");
    }
}
