package event;

/**
 * Base event class.
 */
public class EventHelper {

    private static final EventHelper INSTANCE = new EventHelper();

    private EventHelper() {
    }

    public static EventHelper getInstance() {
        return INSTANCE;
    }

    /**
     * Converts float time to miliseconds.
     * @param timeToPass float time value
     * @return miliseconds time value
     */
    public int timePass(float timeToPass) {
        return (int)(timeToPass * 1000);
    }
}
