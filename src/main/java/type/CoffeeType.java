package type;

/**
 * Holds coffee types as enum.
 */
public enum CoffeeType {

    ESPRESSO("Coffee Espresso", 0.25f),
    LATTE_MACCHIATO("Coffee Latte Macchiato", 0.5f ),
    CAPPUCCINO("Coffee Cappuccino", 0.75f);

    private CoffeeType(String name, float elapsedTime) {
        this.name = name;
        this.elapsedTime = elapsedTime;
    }

    /**
     * Holds human readable name.
     */
    private String name;

    /**
     * Time need to prepare the coffee.
     */
    private float elapsedTime;

    /**
     * Returns name.
     * @return human readable name
     */
    public String getName() {
        return name;
    }

    public float getElapsedTime() {
        return elapsedTime;
    }
}
