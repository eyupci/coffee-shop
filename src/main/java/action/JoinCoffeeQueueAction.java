package action;

import model.Programmer;
import stats.StatsManager;
import type.ProgrammerEventType;

/**
 * Programmer joins to the coffee queue.
 */
public class JoinCoffeeQueueAction extends ProgrammerAction{

    public JoinCoffeeQueueAction(Programmer programmer) {
        super(programmer, ProgrammerEventType.JOIN_COFFEE_QUEUE);
    }

    @Override
    public void process() {
        programmer.getCoffeeShop().getCoffeeManager().addQueue(programmer);

        printInfo();

        StatsManager.getInstance().onProgrammerStart(programmer.getName());
    }


}
