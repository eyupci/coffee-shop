package type;

/**
 * Holds payment types as enum.
 */
public enum PaymentType {

    CASH("cash", 0.25f),
    CREDIT("credit card",0.5f);

    private PaymentType(String name, float elapsedTime) {
        this.name = name;
        this.elapsedTime = elapsedTime;
    }

    /**
     * Human readable name.
     */
    private String name;

    /**
     * Time in seconds.
     */
    private float elapsedTime;

    public float getElapsedTime() {
        return elapsedTime;
    }

    public String getName() {
        return name;
    }
}
