package model;

import executor.ExecutorManager;
import executor.OneTimeJob;
import type.CoffeeType;
import type.PaymentType;
import type.ProgrammerEventType;
import java.util.LinkedList;

/**
 * Holds the programmer
 */
public class Programmer extends OneTimeJob {


    /**
     * Programmer name prefix.
     */
    private String name = "Programmer-";

    /**
     * Programmer action list.
     */
    private LinkedList<ProgrammerEventType> actionList = new LinkedList<>();

    /**
     * Current action index.
     */
    private int actionIndex;

    /**
     * Coffee shop instance.
     */
    private CoffeeShop coffeeShop;

    /**
     * Selected coffee type.
     */
    private CoffeeType selectedCoffeeType;

    /**
     * Selected payment.
     */
    private PaymentType selectedPaymentType;

    public Programmer(CoffeeShop coffeeShop, int index) {
        super(0);
        this.coffeeShop = coffeeShop;
        this.name = name + index;
        actionIndex = -1;
    }

    /**
     * Sets the selected coffee type.
     * @param selectedCoffeeType
     */
    public void setSelectedCoffeeType(CoffeeType selectedCoffeeType) {
        this.selectedCoffeeType = selectedCoffeeType;
    }

    /**
     * Sets the selected payment type.
     * @param selectedPaymentType
     */
    public void setSelectedPaymentType(PaymentType selectedPaymentType) {
        this.selectedPaymentType = selectedPaymentType;
    }

    /**
     * Returns the selected payment type.
     * @return
     */
    public PaymentType getSelectedPaymentType() {
        return selectedPaymentType;
    }

    /**
     * Returns selected coffee type.
     * @return
     */
    public CoffeeType getSelectedCoffeeType() {
        return selectedCoffeeType;
    }

    /**
     * Return coffee shop.
     * @return
     */
    public CoffeeShop getCoffeeShop() {
        return coffeeShop;
    }

    /**
     * Actions to execute are added as ordered.
     */
    public void init() {
        actionList.add(ProgrammerEventType.JOIN_COFFEE_QUEUE);
        actionList.add(ProgrammerEventType.ASK_COFFEE_INFO);
        actionList.add(ProgrammerEventType.PAYMENT_QUEUE);
        actionList.add(ProgrammerEventType.PAY);
        actionList.add(ProgrammerEventType.MACHINE_JOIN_QUEUE);
        actionList.add(ProgrammerEventType.MACHINE_FIND_CUP);
        actionList.add(ProgrammerEventType.MACHINE_PUT);
        actionList.add(ProgrammerEventType.MACHINE_PICK_TYPE);
        actionList.add(ProgrammerEventType.MACHINE_WAIT_TILL_FILL);
        actionList.add(ProgrammerEventType.MACHINE_LEAVE);
    }

    @Override
    public void process() {
        nextAction();
    }

    public void nextAction() {
        actionIndex++;
       if (actionIndex < actionList.size()) {
            ProgrammerEventType eventType = actionList.get(actionIndex);
            ExecutorManager.getInstance().addTask(eventType.getAction(this));
        } else {
            coffeeShop.setDone();
        }
    }

    /**
     * Returns programmer name.
     * @return
     */
    public String getName() {
        return name;
    }
}
