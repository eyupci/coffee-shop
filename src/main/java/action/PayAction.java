package action;

import model.Programmer;
import stats.StatsManager;
import type.ProgrammerEventType;

/**
 * Payment received action.
 */
public class PayAction extends ProgrammerAction {

    public PayAction(Programmer programmer) {
        super(programmer, ProgrammerEventType.PAY,
                programmer.getSelectedPaymentType().getElapsedTime());
    }

    @Override
    public void process() {
        programmer.getCoffeeShop().getCassier().pay(programmer);
        super.process();
        StatsManager.getInstance().onUserPay(programmer.getName(), programmer.getSelectedPaymentType());
    }
}
