package config;

/**
 * Holds environment spesific configuration.
 */
public class Configuration {

    /**
     * Holds maximum number of programmers to pick coffee at the same time.
     */
    private int maxNumberOfPickCoffee;

    /**
     * Holds maximum number of payment at the same time.
     */
    private int maxNumberOfPayment;

    /**
     * Holds maximum number of programmers to get coffee at the same time.
     */
    private int maxNumberOfGetCoffee;

    /**
     * Holds number of programmers.
     */
    private int numberOfProgrammers;


    public Configuration(int maxNumberOfPickCoffee, int maxNumberOfPayment,
                         int maxNumberOfGetCoffee, int numberOfProgrammers) {
        this.maxNumberOfGetCoffee = maxNumberOfGetCoffee;
        this.maxNumberOfPayment = maxNumberOfPayment;
        this.maxNumberOfPickCoffee = maxNumberOfPickCoffee;
        this.numberOfProgrammers = numberOfProgrammers;
    }

    public int getMaxNumberOfGetCoffee() {
        return maxNumberOfGetCoffee;
    }

    public int getMaxNumberOfPayment() {
        return maxNumberOfPayment;
    }

    public int getMaxNumberOfPickCoffee() {
        return maxNumberOfPickCoffee;
    }

    public int getNumberOfProgrammers() {
        return numberOfProgrammers;
    }
}
