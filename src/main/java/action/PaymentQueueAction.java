package action;

import model.Programmer;
import type.ProgrammerEventType;

/**
 * Payment the coffee action
 */
public class PaymentQueueAction extends ProgrammerAction {

    public PaymentQueueAction(Programmer programmer) {
        super(programmer, ProgrammerEventType.PAYMENT_QUEUE);
    }

    @Override
    public void process() {
        this.programmer.getCoffeeShop().getCassier().addQueue(programmer);
        printInfo();
    }
}
