package stats;

import type.CoffeeType;
import type.PaymentType;

/**
 * Basic stats object to measure data.
 */
public final class Stats {

    private String name;

    private CoffeeType coffeeType;

    private long startTime;

    private long endTime;

    private PaymentType paymentType;

    public Stats(String name) {
        this.name = name;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public void setCoffeeType(CoffeeType coffeeType) {
        this.coffeeType = coffeeType;
    }

    public String getName() {
        return name;
    }

    public long getEndTime() {
        return endTime;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setPaymentType(PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    public PaymentType getPaymentType() {
        return paymentType;
    }

    public CoffeeType getCoffeeType() {
        return coffeeType;
    }

    public long getAmountOfTime() {
        return endTime - startTime;
    }
}
