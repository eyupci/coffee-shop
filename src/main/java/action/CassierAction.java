package action;

import executor.PeriodicJob;
import model.Cassier;

import java.util.concurrent.TimeUnit;

/**
 * Simulates the cassier action to get payments.
 */
public class CassierAction extends PeriodicJob {

    /**
     * Holds actual cassier.
     */
    private Cassier cassier;

    private static final int INTERNAL = 100; // in miliseconds

    public CassierAction(Cassier cassier) {
        super(0, INTERNAL, TimeUnit.MILLISECONDS);
        this.cassier = cassier;
    }

    @Override
    public void process() {
        cassier.processPayment();
    }
}
