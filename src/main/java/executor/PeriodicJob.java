package executor;

import java.util.concurrent.TimeUnit;

/**
 * A job that will be executed repeatedly.
 */
public abstract class PeriodicJob extends Job {

    /**
     * Construct setting the start delay and the interval in seconds.
     *
     * @param timeSet start delay in seconds
     * @param interval interval in seconds
     */
    public PeriodicJob(final int timeSet, final int interval) {
        super(timeSet, interval, TimeUnit.SECONDS);
    }

    /**
     * Construct setting the start delay and interval in a given time unit.
     *
     * @param timeSet start delay
     * @param interval interval
     * @param timeUnit time unit to measure
     */
    public PeriodicJob(final int timeSet, final int interval, final TimeUnit timeUnit) {
        super(timeSet, interval, timeUnit);
    }
}
