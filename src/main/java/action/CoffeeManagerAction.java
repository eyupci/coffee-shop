package action;

import executor.PeriodicJob;
import model.CoffeeManager;

import java.util.concurrent.TimeUnit;

/**
 * Responsible from giving the coffee list.
 */
public class CoffeeManagerAction extends PeriodicJob {

    /**
     * Holds coffee manager instance.
     */
    private CoffeeManager coffeeManager;

    /**
     * Holds period interval.
     */
    private static final int INTERVAL = 100;

    public CoffeeManagerAction(CoffeeManager coffeeManager) {
        super(0, INTERVAL,  TimeUnit.MILLISECONDS);
        this.coffeeManager = coffeeManager;
    }

    @Override
    public void process() {
        this.coffeeManager.processCoffeeRequest();
    }
}
